%%
inf_sym = 4;
all_sym = 7;

msg = randerr(1, inf_sym, [0:inf_sym]);
msg_coded = encode(msg, all_sym, inf_sym, 'hamming/binary');
error = randerr(1, all_sym, 1);
msg_received = xor(msg_coded, error);
msg_decoded = decode(msg_received, all_sym, inf_sym, 'hamming/binary');

disp('================');
disp('Source message');
disp(msg);
disp('Coded message');
disp(msg_coded);
disp('Received message');
disp(msg_received);
disp('Decoded message');
disp(msg_decoded);

%%
inf_sym = 4;
all_sym = 7;
indexes = [3 5 6 7 1 2 4];

msg = randerr(1, inf_sym, [1:4]);
mx_gen = [1 0 0 0 1 1 0;
		  0 1 0 0 1 0 1;
		  0 0 1 0 0 1 1;
		  0 0 0 1 1 1 1];

mx_check = [0 1 1;
			1 0 1;
			1 1 0;
			1 1 1;
			0 0 1;
			0 1 0;
			1 0 0];

for i = 1:all_sym
	msg_coded(i) = mod(sum(and(msg, mx_gen(:, i)')), 2);
end

error = randerr(1, all_sym, 1);
msg_received = xor(msg_coded, error);

for i = 1:(all_sym - inf_sym)
	syndrome(i) = ...
		mod(sum(and(msg_received, mx_check(:, i)')), 2);
end

err_index = find(indexes == bi2de(fliplr(syndrome)));
msg_corrected = msg_received;
msg_corrected(err_index) = ~(msg_corrected(err_index));

msg_decoded = msg_corrected(1:4);

disp('================');
disp('Source message:');
disp(msg);
disp('Coded message:');
disp(msg_coded);
disp('Received message:');
disp(msg_received);
disp('Syndrome:');
disp(syndrome);
disp('Decoded Message:');
disp(msg_decoded);

%%
inf_sym = 4;
all_sym = 7;

msg = randerr(1, inf_sym, [0:inf_sym]);
msg_coded = encode(msg, all_sym, inf_sym, 'cyclic/binary');
error = randerr(1, all_sym, 1);
msg_received = xor(msg_coded, error);
msg_decoded = decode(msg_received, all_sym, inf_sym, 'cyclic/binary');

disp('================');
disp('Source message');
disp(msg);
disp('Coded message');
disp(msg_coded);
disp('Received message');
disp(msg_received);
disp('Decoded message');
disp(msg_decoded);

%%
inf_sym = 5;
all_sym = 15;

msg = gf(randerr(1, inf_sym, [0:inf_sym]));
[~, t] = bchgenpoly(all_sym, inf_sym);
msg_coded = bchenc(msg, all_sym, inf_sym);
error = randerr(1, all_sym, t);
msg_received = gf(xor(double(msg_coded.x), error));
msg_decoded = bchdec(msg_received, all_sym, inf_sym);

disp('================');
disp('Source message');
disp(double(msg.x));
disp('Coded message');
disp(double(msg_coded.x));
disp('Received message');
disp(double(msg_received.x));
disp('Decoded message');
disp(double(msg_decoded.x));

%%
bps = 3;
all_sym = 2^bps - 1;
k = 3;

msg = gf([0 7 1; 2 6 3; 4 5 0], bps);
msg_coded = rsenc(msg, all_sym, k);

error = gf([1 0 0 0 0 0 0; 2 1 0 0 0 0 0; 3 1 2 0 0 0 0], bps);
msg_received = msg_coded + error;
msg_decoded = rsdec(msg_received, all_sym, k);

disp('================');
disp('Source message');
disp(double(msg.x));
disp('Coded message');
disp(double(msg_coded.x));
disp('Received message');
disp(double(msg_received.x));
disp('Decoded message');
disp(double(msg_decoded.x));
